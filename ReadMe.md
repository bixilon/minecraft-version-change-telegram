# Minecraft Version Change Telegram

Just a simple bot, that can send you (or a group) a telegram message, when mojang releases a new version of minecraft.

Setup:
1. Create a Telegram channel and add your bot to it
2. Set `TELEGRAM_CHAT_ID` to your public channel id (with prefixed `@`)
3. Set `TELEGRAM_BOT_TOKEN` to your bot token
4. Enjoy

The bot pulls the current version from minecraft every hour.

## Demo
See [@MinecraftVersionChanges](https://t.me/MinecraftVersionChanges)